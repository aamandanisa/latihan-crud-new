<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Kategori;
use File;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();

        return view('berita.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'penulis' => 'required',
            'thumbnail' => 'required |image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
        ]
        );

        $imageName = time().'.'.$request->thumbnail->extension(); //12022022.png

        $request->thumbnail->move(public_path('gambar'),$imageName);

        $berita = new Berita;
        $berita-> judul = $request->judul;
        $berita-> content = $request->content;
        $berita-> penulis = $request->penulis;
        $berita-> kategori_id = $request->kategori_id;
        $berita-> thumbnail = $imageName;
        $berita->save();

        return redirect ('/berita');
        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::find($id);

        return view('berita.show',compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::find($id);
        $kategori = Kategori::all();
        return view('berita.edit',compact('berita','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'penulis' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
        ]
        );
        $berita = Berita::find($id);

        if ($request->has('thumbnail')) {
            //$post->delete();
            $path ='gambar/';
            File::delete($path . $berita->thumbnail);
            $imageName = time().'.'.$request->thumbnail->extension(); //12022022.png

            $request->thumbnail->move(public_path('gambar'),$imageName);

            $berita->thumbnail =$imageName;
            
        }
        $berita-> judul = $request->judul;
        $berita-> content = $request->content;
        $berita-> penulis = $request->penulis;
        $berita-> kategori_id = $request->kategori_id;
        $berita-> thumbnail = $imageName;
        $berita->save();

        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::findorfail($id);
        $berita->delete();

        $path ='gambar/';
        File::delete($path . $berita->thumbnail);

        return redirect('/berita');
    }
}
