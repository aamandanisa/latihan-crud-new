<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@home');
Route::get('/biodata','BiodataController@bio');
Route::post('/welcome','BiodataController@send');

Route::get('/master', function () {
    return view('layout.master');
});

//CRUD Kategori
//Create
//Form input data create kategori
Route::get('/kategori/create','KategoriController@create');
//untuk menyimpan data ke database
Route::post('/kategori','KategoriController@store');

//Read
//menampilkan semua data di table kategori
Route::get('/kategori','KategoriController@index');
//menampilkan detail kategori berdasarkan id
Route::get('/kategori/{kategori_id}','KategoriController@show');

//update
//form edit data edit kategori
Route::get('/kategori/{kategori_id}/edit','KategoriController@edit');
//untuk update data berdasarkan id di table kategori
Route::put('/kategori/{kategori_id}','KategoriController@update');

//Delete
Route::delete('/kategori/{kategori_id}','KategoriController@destroy');

//CRUD Berita
Route::resource('berita','BeritaController');

Auth::routes();

