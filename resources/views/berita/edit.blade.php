@extends('layout.master')

@section('judul')
Edit Berita
@endsection

@section('content')
<form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Judul Berita</label>
      <input type="text" class="form-control"  name="judul" value="{{$berita->judul}}">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Content</label>
    <textarea name="content" class="form-control">{{$berita->content}}</textarea></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
        <label >Penulis Berita</label>
        <input type="text" class="form-control"  name="penulis" value="{{$berita->penulis}}" >
      </div>
      @error('penulis')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group">
        <label >Kategori</label>
        <select name="kategori_id" class="form-control" >
            <option value="">--Pilih Kategori--</option>
            @foreach ($kategori as $item)
                @if($item->id == $berita->kategori_id)

            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
               
                @else
            <option value="{{$item->id}}" >{{$item->nama}}</option>

                    
                @endif
            @endforeach
        </select>
      </div>

      @error('kategori_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group">
        <label >Thumbnail</label>
        <input type="file" class="form-control"  name="thumbnail" >
      </div>
      
      
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
