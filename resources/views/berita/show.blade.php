@extends('layout.master')

@section('judul')
Halaman Detail Berita
@endsection

@section('content')

<div class='row'>
    <div class="col-12">
        <div class="card" >
            <img class="card-img-top" src="{{asset('gambar/'.$berita->thumbnail)}}"  alt="...">
            <div class="card-body">
              <h3 class="card-title">{{$berita->judul}}</h3>
              <p class="card-text">{{ $berita->content}}</p>
              <a href="/berita" class="btn btn-primary">Kembali</a>
            </div>
          </div>
    </div>



</div>

@endsection