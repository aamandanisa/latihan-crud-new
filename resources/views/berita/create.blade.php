@extends('layout.master')

@section('judul')
Tambah Berita
@endsection

@section('content')
<form action="/berita" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >Judul Berita</label>
      <input type="text" class="form-control"  name="judul" >
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
    <label>Content</label>
    <textarea name="content" class="form-control"></textarea></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
        <label >Penulis Berita</label>
        <input type="text" class="form-control"  name="penulis" >
      </div>
      @error('penulis')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group">
        <label >Kategori</label>
        <select name="kategori_id" class="form-control" >
            <option value="">--Pilih Kategori--</option>
            @foreach ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
      </div>

      @error('thumbnail')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group">
        <label >Thumbnail</label>
        <input type="file" class="form-control"  name="thumbnail" >
      </div>
      @error('thumbnail')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
