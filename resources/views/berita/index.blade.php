@extends('layout.master')

@section('judul')
Halaman List Berita
@endsection

@section('content')
<a href="/berita/create" class="btn btn-primary btn-sm my-2">Tambah Berita</a>

<div class='row'>
    @forelse ($berita as $item)
    <div class="col-12">
        <div class="card" >
            <img class="card-img-top" src="{{asset('gambar/'.$item->thumbnail)}}"  width="100%" height="300px" alt="...">
            <div class="card-body">
              <h3>{{$item->judul}}</h3>
              <p class="card-text">{{ Str::limit($item->content,20)}}</p>
            <form action ="/berita/{{$item->id}}" method="post">
                @csrf
                @method('delete')

              
              <a href="/berita/{{$item->id}}" class="btn btn-primary">Detail</a>
              <a href="/berita/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
              <input type="submit" value="delete" class="btn btn-danger">
            </form>
            </div>
          </div>
    </div>


@empty  
    <h1>Tidak ada berita</h1>
@endempty
</div>

@endsection