@extends('layout.master')

@section('judul')
List Kategori
@endsection

@section('content')
<a href="/kategori/create" class="btn btn-primary my-3">Tambah Kategori</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Deskripsi</th>
      </tr>
    </thead>
    <tbody>
        @forelse($kategori as $key=>$item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    
                    <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        
                    </form>
                </td>
            </tr>
        @empty
                      
            <tr>
                <td>Data Tidak Ada</td>
        
            </tr>
        @endforelse
    </tbody>
@endsection
